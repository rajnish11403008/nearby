package suryavanshi.rajnish.com.nearby.Utils;

public class ConfigURLS {

    public static final int Type_1 = 0;
    public static final int  Type_2 = 1;

    private static String LATITUDE,LONGITUDE;

    public static void setLATITUDE(String LATITUDE) {
        ConfigURLS.LATITUDE = LATITUDE;
    }

    private static String PLACE_ID = "place_id";

    public static String getPlaceId() {
        return PLACE_ID;
    }

    public static void setLONGITUDE(String LONGITUDE) {
        ConfigURLS.LONGITUDE = LONGITUDE;
    }

    private static String BASE_URL = "https://maps.googleapis.com/maps/api/";

    private static String PLACE_URL = "place/nearbysearch/json?";

    private static String PLACE_TYPE[] = {"pharmacy","hospital","hotel"};

    private static String RADIUS = "3000";

    private static String LOCATION ;

    public static void setLOCATION() {
        ConfigURLS.LOCATION = LATITUDE+", "+LONGITUDE;
    }

    private static String API_KEY = "AIzaSyArlPYhHDWj0HDf-cyTMNPGqwUZ18rCLPA";

    public static String getPlaceUrl() { return PLACE_URL;   }

    public static String getBaseUrl() {  return BASE_URL;    }

    public static String getPlaceType(int i) {

        return PLACE_TYPE[i];
    }

    public static String getRADIUS() {
        return RADIUS;
    }

    public static String getLOCATION() {
        return LOCATION;
    }

    public static String getAPIKEY() {  return API_KEY;  }


}
