package suryavanshi.rajnish.com.nearby.Fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.airbnb.lottie.LottieAnimationView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import suryavanshi.rajnish.com.nearby.Adapter.MyRecyclerAdapter;
import suryavanshi.rajnish.com.nearby.Data.DataModal;
import suryavanshi.rajnish.com.nearby.R;
import suryavanshi.rajnish.com.nearby.ReviewDetailActivity;
import suryavanshi.rajnish.com.nearby.ReviewDetailDemo;
import suryavanshi.rajnish.com.nearby.Utils.APIClient;
import suryavanshi.rajnish.com.nearby.Utils.APInterface;
import suryavanshi.rajnish.com.nearby.Utils.CheckNetworkConnectivity;
import suryavanshi.rajnish.com.nearby.Utils.ConfigURLS;
import suryavanshi.rajnish.com.nearby.Utils.OpeningHours;
import suryavanshi.rajnish.com.nearby.Utils.Response;
import suryavanshi.rajnish.com.nearby.Utils.ResultsItem;

/**
 * A simple {@link Fragment} subclass.
 */
public class PharmacyFragment extends Fragment {


    private LottieAnimationView lottieAnimationView;

    List<DataModal> dataModalList;
    APInterface apInterface;

    public RecyclerView recyclerView;

    View view;
    public PharmacyFragment() {
        // Required empty public constructor


    }

    private void init(final View view) {
        this.view = view;
        dataModalList = new ArrayList<>();
        recyclerView = (RecyclerView)view.findViewById(R.id.pharmacy_recycler);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        lottieAnimationView = (LottieAnimationView)view.findViewById(R.id.loading);
        lottieAnimationView.setVisibility(View.VISIBLE);
        lottieAnimationView.playAnimation();

        apInterface = APIClient.getClient().create(APInterface.class);

        Call<Response> call = apInterface.getDataResponse(ConfigURLS.getLOCATION(),
                ConfigURLS.getRADIUS(),
                ConfigURLS.getPlaceType(0),
                ConfigURLS.getAPIKEY());

        call.enqueue(new Callback<Response>() {
            @Override
            public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                Response dataResponse = response.body();
                if(response.isSuccessful() && dataResponse.getStatus().toUpperCase().equals("OK")){
                    List<ResultsItem> resultsItemList = dataResponse.getResults();
                    displayData(resultsItemList);
                }
                else if(dataResponse.getStatus().toUpperCase().equals("OVER_QUERY_LIMIT")){
                    Snackbar.make(view,"Error "+dataResponse.getStatus()+" found",Snackbar.LENGTH_LONG).show();
                }
                else if(response.code()!=200){
                    Snackbar.make(view,"Error "+response.code()+" found",Snackbar.LENGTH_LONG).show();

                }
            }

            @Override
            public void onFailure(Call<Response> call, Throwable t) {
                lottieAnimationView.pauseAnimation();
                lottieAnimationView.setVisibility(View.GONE);
                call.cancel();
                t.printStackTrace();
            }

        });
    }

    private void displayData(List<ResultsItem> resultsItemList) {

        for (int i = 0;i<resultsItemList.size();i++){
            DataModal dataModal = new DataModal();

            dataModal.setPlace_id(resultsItemList.get(i).getPlaceId());

            dataModal.setName(resultsItemList.get(i).getName());

            OpeningHours available = resultsItemList.get(i).getOpeningHours();
            if(available!=null){
                if(available.isOpenNow())   {  dataModal.setOpeningHours(0);  }
                else    {  dataModal.setOpeningHours(1);      }
            }
            else{    dataModal.setOpeningHours(2);      }

            dataModal.setRatingStatus(resultsItemList.get(i).getRating());

            dataModalList.add(dataModal);
            //textView.append(resultsItemList.get(i).getName());
        }

        Collections.sort(dataModalList,DataModal.ratingComparator);
        lottieAnimationView.pauseAnimation();
        lottieAnimationView.setVisibility(View.GONE);
        MyRecyclerAdapter adapter = new MyRecyclerAdapter(getContext(),dataModalList);
        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        adapter.setOnItemClikListener(new MyRecyclerAdapter.ClickListerner() {
            @Override
            public void OnItemClick(int Position, View view) {

                Intent intent = new Intent(getContext(),ReviewDetailDemo.class);
                Log.i("Pharmacy", "OnItemClick: "+dataModalList.get(Position).getPlace_id());
                intent.putExtra(ConfigURLS.getPlaceId(),dataModalList.get(Position).getPlace_id());
                startActivity(intent);
            }

            @Override
            public void OnItemLongClick(int Position, View view) {
                Log.d("RecyclerView", "onItemLongClick pos = " + Position);
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View pharmacy_view = inflater.inflate(R.layout.fragment_nearme, container, false);
        init(pharmacy_view);
        return pharmacy_view;
    }
}
