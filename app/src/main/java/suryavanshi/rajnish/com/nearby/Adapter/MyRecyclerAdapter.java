package suryavanshi.rajnish.com.nearby.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import me.zhanghai.android.materialratingbar.MaterialRatingBar;
import suryavanshi.rajnish.com.nearby.Data.DataModal;
import suryavanshi.rajnish.com.nearby.R;

public class MyRecyclerAdapter extends RecyclerView.Adapter<MyRecyclerAdapter.MyHolder> {

    private  ClickListerner clickListerner;

    public interface ClickListerner{
        void OnItemClick(int Position,View view);
        void OnItemLongClick(int Position,View view);
    }

    List<DataModal> dataModalList;
    Context context;

    /**
     *
     * @param context
     * @param dataModalList
     */
    public MyRecyclerAdapter(Context context,List<DataModal> dataModalList){
        this.context = context;
        this.dataModalList = dataModalList;
    }

     @NonNull
    @Override
    public MyHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_layout,parent,false);

        return new MyHolder(view);
    }


    @Override
    public void onBindViewHolder(@NonNull MyHolder holder, int position) {

        DataModal modal = dataModalList.get(position);
        holder.pharmacyName.setText(modal.getName());
        holder.openingHours.setText(modal.getOpeningHours());
        holder.ratingBar.setRating((float)modal.getRatingStatus());
    }

    @Override
    public int getItemCount() {
        return dataModalList.size();
    }


    public class MyHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener,View.OnLongClickListener{

        TextView pharmacyName;
        TextView openingHours;
        MaterialRatingBar ratingBar;
        /**
         *
         * @param itemView
         */
        public MyHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            itemView.setOnClickListener(this);
            pharmacyName = (TextView)itemView.findViewById(R.id.pharmacy_name);
            openingHours = (TextView)itemView.findViewById(R.id.opening_hours);
            ratingBar = (MaterialRatingBar)itemView.findViewById(R.id.rating);
        }

        @Override
        public void onClick(View v) {
            clickListerner.OnItemClick(getAdapterPosition(),v);
        }

        @Override
        public boolean onLongClick(View v) {
            clickListerner.OnItemLongClick(getAdapterPosition(),v);
            return false;
        }
    }

    public  void setOnItemClikListener(ClickListerner clickListerner) {
        this.clickListerner = clickListerner;
    }
}
