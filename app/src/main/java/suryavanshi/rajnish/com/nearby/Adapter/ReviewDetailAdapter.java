package suryavanshi.rajnish.com.nearby.Adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.apache.commons.lang3.StringUtils;

import java.util.List;

import suryavanshi.rajnish.com.nearby.R;
import suryavanshi.rajnish.com.nearby.Utils.Review_Utils.ReviewsItem;

public class ReviewDetailAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    List<ReviewsItem> reviewsItemList;
    public ReviewDetailAdapter(List<ReviewsItem> reviewsItemList){
        this.reviewsItemList = reviewsItemList;
    }
    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.review_item_layout,parent,false);
        viewHolder = new ReviewHolderClass(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ReviewHolderClass reviewHolder = (ReviewHolderClass)holder;
        configureReviewHolder(reviewHolder,position);
    }

    private void configureReviewHolder(ReviewHolderClass reviewHolder, int position) {
        ReviewsItem reviewResult = (ReviewsItem) reviewsItemList.get(position);

        reviewHolder.authorName.setText(StringUtils.capitalize(reviewResult.getAuthorName()));
        reviewHolder.desc.setText(reviewResult.getText());
        reviewHolder.ratingBar.setRating(reviewResult.getRating());
        reviewHolder.time.setText(String.valueOf(reviewResult.getTime()));
    }

    @Override
    public int getItemCount() {
        return reviewsItemList.size();
    }
}
