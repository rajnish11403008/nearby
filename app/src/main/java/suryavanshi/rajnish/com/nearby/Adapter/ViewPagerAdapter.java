package suryavanshi.rajnish.com.nearby.Adapter;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.Log;

import suryavanshi.rajnish.com.nearby.Fragment.HospitalFragment;
import suryavanshi.rajnish.com.nearby.Fragment.HotelFragment;
import suryavanshi.rajnish.com.nearby.Fragment.PharmacyFragment;
import suryavanshi.rajnish.com.nearby.R;

public class ViewPagerAdapter extends FragmentStatePagerAdapter {

    private  Context context;

    public ViewPagerAdapter(Context context,FragmentManager fm) {
        super(fm);
        this.context = context;

    }

    @Override
    public Fragment getItem(int position) {
        Log.i(""+position, "getItem: ");
        switch (position){
            case 0: return new PharmacyFragment();
            case 1: return new HospitalFragment();
            case 2: return new HotelFragment();
        }
        return null;
    }

    @Override
    public int getCount() {
        return context.getResources().getStringArray(R.array.page_title).length;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        String page_title[] = context.getResources().getStringArray(R.array.page_title);

        switch (position){
            case 0: return page_title[0];
            case 1: return page_title[1];
            case 2: return page_title[2];
        }
        return null;
    }
}
