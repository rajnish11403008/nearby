package suryavanshi.rajnish.com.nearby.Adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import suryavanshi.rajnish.com.nearby.R;

public class WeekDayHolderClass extends RecyclerView.ViewHolder {

    TextView weekDay[] = new TextView[7];
    int weekid[] = {R.id.mon,R.id.tue,R.id.wed,R.id.thu,R.id.fri,R.id.sat,R.id.sun};
    private View view;
    public WeekDayHolderClass(View itemView) {
        super(itemView);
       for(int i = 0;i<7;i++){
           weekDay[i] = (TextView)itemView.findViewById(weekid[i]);
       }
    }
}
