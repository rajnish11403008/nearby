package suryavanshi.rajnish.com.nearby.Data;

import java.util.Comparator;

public class DataModal {

    private String place_id;

    private String name;

    private String openingHours;

    private double ratingStatus;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOpeningHours(){
        return openingHours;
    }

    public void setOpeningHours(int flag){
        switch (flag){
            case 0:     openingHours = "Open Now";
                        break;
            case 1:     openingHours = "Closed Now";
                        break;
            default:    openingHours = "Not Available";
                        break;
        }
    }

    public String getPlace_id() {
        return place_id;
    }

    public void setPlace_id(String place_id) {
        this.place_id = place_id;
    }

    public double getRatingStatus() {
        return ratingStatus;
    }

    public void setRatingStatus(double ratingStatus) {
        this.ratingStatus = ratingStatus;
    }

   public static Comparator<DataModal> ratingComparator = new Comparator<DataModal>() {
       @Override
       public int compare(DataModal o1, DataModal o2) {
           if(o1.getRatingStatus()<o2.getRatingStatus()) return 1;
           if(o1.getRatingStatus()>o2.getRatingStatus()) return -1;
           else return 0;
       }
   };
}
