package suryavanshi.rajnish.com.nearby;


import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import suryavanshi.rajnish.com.nearby.API.LocationTrack;
import suryavanshi.rajnish.com.nearby.Adapter.ViewPagerAdapter;
import suryavanshi.rajnish.com.nearby.Utils.CheckNetworkConnectivity;
import suryavanshi.rajnish.com.nearby.Utils.ConfigURLS;


public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener{

    protected String latitude, longitude;

    LocationTrack locationTrack;
    private ViewPager viewPager;
    private TabLayout layout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        viewPager = (ViewPager)findViewById(R.id.content_main_pager);
         layout = (TabLayout)findViewById(R.id.tablayout);


        getLocation();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();



        /*
            Setting ViewPager With PagerAdapter and Tablayout
         */

        setUpPager();

    }

    private void setUpPager() {
        if(new CheckNetworkConnectivity(MainActivity.this).isNetworkAvailable()){
            NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
            navigationView.setNavigationItemSelectedListener(this);
            ViewPagerAdapter adapter = new ViewPagerAdapter(this,getSupportFragmentManager());
            viewPager.setAdapter(adapter);
            layout.setupWithViewPager(viewPager);

            viewPager.setOffscreenPageLimit(3);
        }
        else{

            final View networkError = findViewById(R.id.connectivity_layout);
            networkError.setVisibility(View.VISIBLE);
            TextView btn_refresh = (TextView) networkError.findViewById(R.id.btn_refresh);
            btn_refresh.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    networkError.setVisibility(View.GONE);
                    setUpPager();
                }
            });
        }
    }

    private void getLocation() {

        locationTrack = new LocationTrack(MainActivity.this);

        if(locationTrack.canGetLocation()) {
            latitude = String.valueOf(locationTrack.getLatitude());
            longitude = String.valueOf(locationTrack.getLongitude());
            Log.i( "getLocation: ",latitude+","+longitude);
            ConfigURLS.setLATITUDE(latitude);
            ConfigURLS.setLONGITUDE(longitude);
            ConfigURLS.setLOCATION();
        }
        else {
            locationTrack.showSettingsAlert();
        }

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {

            finish();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_pharmacy) {
            // Handle the camera action
            viewPager.setCurrentItem(0);
        } else if (id == R.id.nav_hospital) {
            viewPager.setCurrentItem(1);
        } else if (id == R.id.nav_hotel) {
            viewPager.setCurrentItem(2);
        }  else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

}
