package suryavanshi.rajnish.com.nearby.Adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.apache.commons.lang3.StringUtils;

import java.util.List;
import suryavanshi.rajnish.com.nearby.R;
import suryavanshi.rajnish.com.nearby.Utils.Review_Utils.OpeningHours;
import suryavanshi.rajnish.com.nearby.Utils.Review_Utils.Result;
import suryavanshi.rajnish.com.nearby.Utils.Review_Utils.ReviewsItem;

public class ComplexRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private List<Object> objectList;

    private final int reviewData = 0, weekData = 1;
    public ComplexRecyclerAdapter(List<Object> objectList) {
        this.objectList = objectList;
        Log.i( "ComplexRecyclerAdapter: ",objectList.toString());
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        switch (viewType){
            case weekData:
                View view1 = inflater.inflate(R.layout.weekday_timing_layout,parent,false);
                viewHolder = new WeekDayHolderClass(view1);
                break;
            default:
                View view2 = inflater.inflate(R.layout.review_item_layout,parent,false);
                viewHolder = new ReviewHolderClass(view2);
                break;
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        switch (position){
            case 0:
                Log.i("onBindViewHolder: ","case 0");
                WeekDayHolderClass holder1 = (WeekDayHolderClass)holder;
                configureWeekdayHolder(holder1,position);
                break;
            default:
                Log.i("onBindViewHolder: ","case default");
                ReviewHolderClass holder2 = (ReviewHolderClass)holder;
                configureReviewHolder(holder2,position);
                break;
        }

    }

    private void configureReviewHolder(ReviewHolderClass holder2, int position) {
        ReviewsItem reviewResult = (ReviewsItem) objectList.get(position);

            holder2.authorName.setText(StringUtils.capitalize(reviewResult.getAuthorName()));
            holder2.desc.setText(reviewResult.getText());
            holder2.ratingBar.setRating(reviewResult.getRating());
            holder2.time.setText(String.valueOf(reviewResult.getTime()));


    }

    private void configureWeekdayHolder(WeekDayHolderClass holder1, int position) {
        OpeningHours openingHours = (OpeningHours)objectList.get(position);
        if(openingHours!=null){
            List<String> weekDayList = openingHours.getWeekdayText();
            for(int i = 0 ;i<weekDayList.size();i++){
                String time = weekDayList.get(i);
                holder1.weekDay[i].setText(time.substring(time.indexOf(":")+1));
            }
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0 ){
            return weekData;
        }
        else{
            return reviewData;
        }
    }

    @Override
    public int getItemCount() {
        return objectList.size();
    }
}
