package suryavanshi.rajnish.com.nearby.Utils;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class APIClient {

    private static Retrofit retrofit = null;

     public static Retrofit getClient(){

         HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
         interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
         OkHttpClient client = new OkHttpClient.Builder()
                 .readTimeout(30,TimeUnit.SECONDS)
                 .writeTimeout(30,TimeUnit.SECONDS)
                 .addInterceptor(interceptor).build();

         if(retrofit == null){
             retrofit = new Retrofit.Builder()
                     .baseUrl(ConfigURLS.getBaseUrl())
                     .addConverterFactory(GsonConverterFactory.create())
                     .client(client)
                     .build();
         }
         return retrofit;
     }
}
