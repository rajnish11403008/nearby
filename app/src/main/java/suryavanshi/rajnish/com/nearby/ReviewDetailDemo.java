package suryavanshi.rajnish.com.nearby;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import suryavanshi.rajnish.com.nearby.Adapter.ComplexRecyclerAdapter;
import suryavanshi.rajnish.com.nearby.Adapter.ReviewDetailAdapter;
import suryavanshi.rajnish.com.nearby.Utils.APIClient;
import suryavanshi.rajnish.com.nearby.Utils.APInterface;
import suryavanshi.rajnish.com.nearby.Utils.CheckNetworkConnectivity;
import suryavanshi.rajnish.com.nearby.Utils.ConfigURLS;
import suryavanshi.rajnish.com.nearby.Utils.Review_Utils.OpeningHours;
import suryavanshi.rajnish.com.nearby.Utils.Review_Utils.ReviewResponse;
import suryavanshi.rajnish.com.nearby.Utils.Review_Utils.ReviewsItem;

public class ReviewDetailDemo extends AppCompatActivity {

    private RecyclerView recyclerView;
    private ReviewDetailAdapter adapter;
    private APInterface apInterface;
    private LottieAnimationView lottieAnimationView;
    private String placeId = null;
    private List<ReviewsItem> reviewsItemList;
    private TextView mon,tue,wed,thu,fri,sat,sun;
    private TextView weekDay[] = new TextView[7];
    private int weekid[] = {R.id.mon,R.id.tue,R.id.wed,R.id.thu,R.id.fri,R.id.sat,R.id.sun};
    private View connectivity_layout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_review_detail_demo);
        connectivity_layout = findViewById(R.id.connectivity_layout);
        reviewsItemList = new ArrayList<>();
        lottieAnimationView = (LottieAnimationView)findViewById(R.id.loading);
        recyclerView = (RecyclerView)findViewById(R.id.review_recycler);
        for(int i = 0;i<7;i++){
            weekDay[i] = (TextView)findViewById(weekid[i]);
        }
        checkConnectivity();



    }

    private void checkConnectivity(){
        if(new CheckNetworkConnectivity(ReviewDetailDemo.this).isNetworkAvailable()){
            lottieAnimationView.setVisibility(View.VISIBLE);
            lottieAnimationView.playAnimation();
            placeId = getIntent().getStringExtra(ConfigURLS.getPlaceId());
            init();
        }
        else{
            connectivity_layout.setVisibility(View.VISIBLE);
            TextView retry = (TextView)connectivity_layout.findViewById(R.id.btn_refresh);
            retry.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    connectivity_layout.setVisibility(View.GONE);
                    checkConnectivity();
                }
            });

        }
    }
    private void init() {
        apInterface = APIClient.getClient().create(APInterface.class);
        Call<ReviewResponse> call = apInterface.getReviewResponse(placeId,ConfigURLS.getAPIKEY());

        call.enqueue(new Callback<ReviewResponse>() {
            @Override
            public void onResponse(Call<ReviewResponse> call, Response<ReviewResponse> response) {
                ReviewResponse reviewResponse = response.body();



                if(response.isSuccessful() && reviewResponse.getStatus().toUpperCase().equals("OK")){
                    displayData(reviewResponse);
                }
                else if(reviewResponse.getStatus().toUpperCase().equals("OVER_QUERY_LIMIT")){
                    Toast.makeText(getApplicationContext(),"Over Query Limit",Toast.LENGTH_SHORT).show();
                }
                else if(response.code()!=200){
                    Toast.makeText(getApplicationContext(),"Request "+response.code()+" Code Found",Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ReviewResponse> call, Throwable t) {
                lottieAnimationView.pauseAnimation();
                lottieAnimationView.setVisibility(View.GONE);
                call.cancel();
                t.printStackTrace();
            }
        });
    }

    private void displayData(ReviewResponse reviewResponse) {
       reviewsItemList = reviewResponse.getResult().getReviews();
        OpeningHours openingHours = reviewResponse.getResult().getOpeningHours();
        if(openingHours!=null){
            List<String> weekDayList = openingHours.getWeekdayText();
            for(int i = 0 ;i<weekDayList.size();i++){
                String time = weekDayList.get(i);
                weekDay[i].setText(time.substring(time.indexOf(":")+1));
            }
        }

        lottieAnimationView.setVisibility(View.GONE);
        lottieAnimationView.pauseAnimation();
        adapter = new ReviewDetailAdapter(reviewsItemList);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }
}
