package suryavanshi.rajnish.com.nearby.Utils;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Query;
import suryavanshi.rajnish.com.nearby.Utils.Review_Utils.ReviewResponse;

public interface APInterface {

    @GET("place/nearbysearch/json?")
    Call<Response> getDataResponse(@Query(value = "location",encoded = true) String location,
                                   @Query(value = "radius", encoded = true) String radius,
                                   @Query(value = "type", encoded = true) String type,
                                   @Query(value = "key", encoded = true) String key);


    @GET("place/details/json?")
    Call<ReviewResponse> getReviewResponse(@Query(value = "placeid", encoded = true) String placeId,
                                           @Query(value = "key", encoded = true) String key);
}
