package suryavanshi.rajnish.com.nearby.Utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkInfo;
import android.os.Build;

public class CheckNetworkConnectivity {
    private Context context;

    public CheckNetworkConnectivity(Context context) {
        this.context = context;
    }

    public boolean isNetworkAvailable(){
        if(context!=null){
            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                // return Settings.Global.getInt(context.getContentResolver(),"mobile_data",0)==1;
                Network[] networks = cm.getAllNetworks();
                NetworkInfo networkInfo;

                for (Network mNetwork : networks) {

                    networkInfo = cm.getActiveNetworkInfo();
                    // Log.i("isNetworkAvailable: ",networkInfo.toString());
                    if (networkInfo!=null && networkInfo.getState().equals(NetworkInfo.State.CONNECTED)) {
                        return true;
                    }
                }
            } else {
                if (cm != null)
                {
                    //noinspection deprecation
                    NetworkInfo[] info = cm.getAllNetworkInfo();
                    if (info != null) {
                        // Log.i( "isNetworkAvailable: ",info.toString());
                        for (NetworkInfo anInfo : info) {
                            if (anInfo.getState().equals(NetworkInfo.State.CONNECTED)) {
                                return true;
                            }
                        }
                    }
                }
            }
        }
        return false;
    }
}
