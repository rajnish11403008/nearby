package suryavanshi.rajnish.com.nearby.Adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RatingBar;
import android.widget.TextView;

import me.zhanghai.android.materialratingbar.MaterialRatingBar;
import suryavanshi.rajnish.com.nearby.R;

public class ReviewHolderClass extends RecyclerView.ViewHolder {


    TextView authorName,desc,time;
    MaterialRatingBar ratingBar;
    View view;
    public ReviewHolderClass(View itemView) {
        super(itemView);
        authorName = (TextView)itemView.findViewById(R.id.authorName);
        desc = (TextView)itemView.findViewById(R.id.review_desc);
        ratingBar = (MaterialRatingBar)itemView.findViewById(R.id.review_rating);
        time = (TextView)itemView.findViewById(R.id.time_text1);
    }
}
